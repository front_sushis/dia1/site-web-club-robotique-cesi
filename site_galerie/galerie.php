<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Galerie</title>
</head>

<body>
    <header>
        <?php include '../site_structure/header.php' ?> <!--Cherche le header.php dans le dossier site_structure-->
    </header>
    <div class="main">
        <?php include 'galerie.html' ?> <!--Importe galerie.html-->
        <?php include '../site_structure/bouton.html' ?> <!--Cherche le bouton.html dans le dossier site_structure-->
    </div>
    <footer>
        <?php include '../site_structure/footer.php' ?> <!--Cherche le footer.php dans le dossier site_structure-->
    </footer>
</body>
</html>

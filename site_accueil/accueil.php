<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../site_structure/header.css" />
    <link rel="stylesheet" href="../site_structure/footer.css" />
    <title>Accueil</title>
</head>
<body>
    <header>
        <?php include '../site_structure/header.php' ?> <!--Importe le header.php dans le dossier site_structure-->
    </header>
    <?php include 'accueil.html' ?> <!--Importe equipe.php-->
    <?php include '../site_structure/bouton.html' ?> <!--Importe le bouton.html dans le dossier site_structure-->
    <footer>
        <?php include '../site_structure/footer.php' ?> <!--Importe le footer.php dans le dossier site_structure-->
    </footer>
</body>
</html>

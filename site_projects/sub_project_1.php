<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="sub_project.css" /> <!--importe le fichier equipe.css-->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
        href="https://fonts.googleapis.com/css2?family=Source+Code+Pro:wght@300&family=Ubuntu:wght@300;400&display=swap"
        rel="stylesheet" />
    <!--importe la video de fond-->
    <video id="background-video" autoplay loop muted>
        <source src="../rsrc_videos/background.mp4" type="video/mp4" />
    </video>
    <title>Projet 1</title>
</head>

<body>
<header>
        <?php
            include '../site_structure/header.php'; //importe le fichier equipe.css
        ?>
    </header>
    <div class="insight_projects">
      <div class="nav_projects">
        <img src="../rsrc_images/illustration_screen.jpg" id="pic_project" />
        <img src="../rsrc_images/illustration_screen.jpg" id="pic_project" />
        <img src="../rsrc_images/illustration_screen.jpg" id="pic_project" />
        <img src="../rsrc_images/illustration_screen.jpg" id="pic_project" />
      </div>
      
      <div class="resume_project">
        <!--Le screen aspect concerne la partie avec le titre(top_tabs) et les differents contenues-->

        <div class="screen_aspect" id="projet1">
          <div class="top_tabs">
            <div class="active_tab">
              Projet Sumo
              <img src="../rsrc_icones/robot1.png" id="cross_close" />
            </div>
            <div class="inactive_tab"></div>
          </div>
          <div class="container_text">
            <div class="event_present">
              <!--Contient les descriptions des projets-->
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Suspendisse venenatis magna nulla, feugiat egestas ligula
                venenatis sit amet. Pellentesque volutpat, dolor id sollicitudin
                semper, turpis tortor consectetur elit, vel luctus. Lorem ipsum
                dolor sit amet, consectetur adipiscing elit. Suspendisse
                venenatis magna nulla, feugiat egestas ligula venenatis sit
                amet. Pellentesque volutpat, dolor id sollicitudin semper,
                turpis tortor consectetur elit, vel luctus.
              </p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Suspendisse venenatis magna nulla, feugiat egestas ligula
                venenatis sit amet. Pellentesque volutpat, dolor id sollicitudin
                semper, turpis tortor consectetur elit, vel luctus. Lorem ipsum
                dolor sit amet, consectetur adipiscing elit. Suspendisse
                venenatis magna nulla, feugiat egestas ligula venenatis sit
                amet. Pellentesque volutpat, dolor id sollicitudin semper,
                turpis tortor consectetur elit, vel luctus.
              </p>
              <progress value="50" max="100"></progress>
            </div>
          </div>
        </div>

        <div class="advice_members">
          <div class="screen_aspect">
            <div class="top_tabs">
              <div class="active_tab">
                Membre
                <img src="../rsrc_icones/robot1.png" id="cross_close" />
              </div>
            </div>
            <div class="container_text">
              <div class="event_present">
                <!--Contient les descriptions des projets-->
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Suspendisse venenatis magna nulla, feugiat egestas ligula
                  venenatis sit amet. Pellentesque volutpat, dolor id
                  sollicitudin semper, turpis tortor consectetur elit, vel
                  luctus. Lorem ipsum dolor sit amet, consectetur adipiscing
                  elit. Suspendisse venenatis magna nulla, feugiat egestas
                  ligula venenatis sit amet. Pellentesque volutpat, dolor id
                  sollicitudin semper, turpis tortor consectetur elit, vel
                  luctus.
                </p>
              </div>
            </div>
          </div>

          <div class="screen_aspect">
            <div class="top_tabs">
              <div class="active_tab">
                Membre
                <img src="../rsrc_icones/robot1.png" id="cross_close" />
              </div>
            </div>
            <div class="container_text">
              <div class="event_present">
                <!--Contient les descriptions des projets-->
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Suspendisse venenatis magna nulla, feugiat egestas ligula
                  venenatis sit amet. Pellentesque volutpat, dolor id
                  sollicitudin semper, turpis tortor consectetur elit, vel
                  luctus. Lorem ipsum dolor sit amet, consectetur adipiscing
                  elit. Suspendisse venenatis magna nulla, feugiat egestas
                  ligula venenatis sit amet. Pellentesque volutpat, dolor id
                  sollicitudin semper, turpis tortor consectetur elit, vel
                  luctus.
                </p>
              </div>
            </div>
          </div>

          <div class="screen_aspect">
            <div class="top_tabs">
              <div class="active_tab">
                Membre
                <img src="../rsrc_icones/robot1.png" id="cross_close" />
              </div>
            </div>
            <div class="container_text">
              <div class="event_present">
                <!--Contient les descriptions des projets-->
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Suspendisse venenatis magna nulla, feugiat egestas ligula
                  venenatis sit amet. Pellentesque volutpat, dolor id
                  sollicitudin semper, turpis tortor consectetur elit, vel
                  luctus. Lorem ipsum dolor sit amet, consectetur adipiscing
                  elit. Suspendisse venenatis magna nulla, feugiat egestas
                  ligula venenatis sit amet. Pellentesque volutpat, dolor id
                  sollicitudin semper, turpis tortor consectetur elit, vel
                  luctus.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<?php include '../site_structure/bouton.html' ?> <!--Importe le bouton.html dans le dossier site_structure-->
    <footer>
        <?php
            include '../site_structure/footer.php'; 
        ?><!--Importe le footer.php dans le dossier site_structure-->
    </footer>

    </body>

</html>
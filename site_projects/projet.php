<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../site_structure/header.css" />
    <link rel="stylesheet" href="../site_structure/footer.css" />
    <title>Projets</title>
</head>
<header>
    <?php include '../site_structure/header.php' ?> <!--Importe le header.php dans le dossier site_structure-->
</header>
<body>
    <?php include 'projets.html' ?> <!--Importe projets.html-->
    <?php include '../site_structure/bouton.html' ?> <!--Importe le bouton.html dans le dossier site_structure-->
</body>
<footer>
    <?php include '../site_structure/footer.php' ?> <!--Importe le footer.php dans le dossier site_structure-->
</footer>
</html>

# Site_Web_Robotique_CESI

Dans le cadre de notre projet de validation du bloc concernant la création d'un site Web et de son application mobile (responsive), nous avons réalisé le site internet du Club de Robotique de CESI Brest.

Le site est géré en : 
- HTML
- CSS
- PHP
- Quelques notions de JS

## Organisation 

- Dossiers contenant les ressources extérieures : rsrc_XXXX
- Un dossier par page internet réalisée
- Un dossier de structure qui contient la plupart des éléments récurrents de chaque page
- Un fichier CSS et PHP par page

## Contexte du projet 

- Réaliser par groupe de trois
- Langages de programmation imposés
- Rajout de la partie PHP
- Possibilité d'utiliser du JS

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../site_structure/footer.css" />
    <title>Footer</title>
  </head>
  <footer>
    <!-- Concerne l'adresse du club -->
    <div class="club">
      <p>Club de Robotique CESI Brest</p>
      <p>230 rue Rolland Garros</p>
      <p>29490, Guipavas</p>
    </div>
    <!-- Concerne les réseaux sociaux -->
    <div class="social_network">
        <a href="https://twitter.com"> <img src="../rsrc_icones/footer_twitter.png" id="social" width="40em"><span class="nonemobile">Suis-nous sur</span><span> Twitter</span></a>
        <a href="https://instagram.com"> <img src="../rsrc_icones/footer_instagram.png" id="social" width="40em"><span class="nonemobile">Retrouve-nous sur</span><span> Instagram</span></a>
        <a href="https://discord.com"> <img src="../rsrc_icones/footer_discord.png" id="social" width="40em"><span class="nonemobile">Propose tes idées sur</span><span> Discord</span></a>
        <a href="../site_contact/contact.php"> <img src="../rsrc_icones/footer_mail.png" id="social" width="40m"><span class="nonemobile">Envoie-nous un</span><span> mail</span></a>
  </div>
    <!-- Concerne les partenaires, mettre à jour l'adresse de l'icone et le href -->
    <div class="partnership">
      <div class="logo_partner">
        <a href="https://brest.cesi.fr" target="_blank"> <img src="../rsrc_icones/footer_cesi.png" id="logo" width="40em"></a>
        <a href="#"> <img src="../rsrc_icones/icon_partner.png" id="logo" width="40em"></a>
        <a href="#"> <img src="../rsrc_icones/icon_partner.png" id="logo" width="40em"></a>
        <a href="#"> <img src="../rsrc_icones/icon_partner.png" id="logo" width="40em"></a>
      </div>
    </div>
  </footer>
</html>

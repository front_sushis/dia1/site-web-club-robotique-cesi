let btn1 = document.getElementById("pic_hamburger");
let menu1 = document.getElementById("navigation");
let menu2 = document.getElementById("sous-menu");

function OpenNav() { //Fonction pour ouvrir (et fermer) le menu hamburger sur version telephone
  if (getComputedStyle(menu1).visibility != "visible") {
    menu1.style.visibility = "visible";
    menu2.style.display = "contents";
  } else {
    menu1.style.visibility = "hidden";
    menu2.style.display = "none";
  }
}

btn1.onclick = OpenNav; // La fonction se declanche au clique

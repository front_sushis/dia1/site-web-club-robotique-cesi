<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../site_structure/header.css" />
    <title>Header</title>
  </head>

  <body>
    <header>
      <nav>
        <!-- Pour le menu si c'est un téléphone -->
        <div class = "hamburger">
            <img src="../rsrc_icones/icon_hamburger.png" alt ="Menu hamburger" id="pic_hamburger"/>
        </div>
        <!-- Pour le menu si c'est un ordinateur -->
        <ul id="navigation">
          <li class="scrolling_menu">
            <a id="presentation">Présentation</a>
            <!-- Pour le sous menu, gère les listes de navigation, permet de rajouter facilement -->
            <ul id="sous-menu">
              <li><a href="../site_accueil/accueil.php">Qui sommes-nous ?</a></li>
              <li><a href='../site_equipe/equipe.php'>L'équipe</a></li>
              <li><a a href='../site_contact/contact.php'>Contactez-nous</a></li>
            </ul>
          </li>
          <li><a href='../site_events/evenement.php'>Evènements</a></li>
          <li><a href='../site_projects/projet.php'>Projets</a></li>
          <li><a href='../site_galerie/galerie.php'>Galerie</a></li>
        </ul>
      </nav>
      <!-- Apparence du logo et du nom du club -->
      <div class="logo_name">
        <a href='../site_accueil/accueil.php'><img src="../rsrc_icones/header_robot.png" id="logo_robot" /></a>
        <div class="club_cesi"><a href='../site_accueil/accueil.php'>CLUB ROBOTIQUE CESI</a></div>
      </div>
    </header>
  </body>
</html>

<script> <?php include '../site_structure/header.js' ?> </script>

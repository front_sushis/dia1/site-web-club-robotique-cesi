<?php
//Je recupere les infos de ma base de données
$serveur = "localhost";
$dbname = "form_robotique";
$user = "root";
$pass = "admin";

//Je verifie que toutes les cases du formulaire ont étés remplis (qu'elles ne sont pas vide)

if (!empty($_POST["nom"]) && !empty($_POST["prenom"]) && !empty($_POST["mail"]) && !empty($_POST["msg"])){
    //si aucune case n'est vide je remplis mes variables 
    $nom = $_POST["nom"]; 
    $prenom = $_POST["prenom"];
    $mail = $_POST["mail"];
    $msg = $_POST["msg"];
}else{ 
    //sinon je renvoi un message d'erreur via une popup ('alert') et je renvoi l'utilisateur sur la page contact
    ?>
    <script>
        alert("<?php echo htmlspecialchars('Veuillez remplir tous les champs', ENT_QUOTES); ?>")  
        window.location = '../site_contact/contact.php'; 
    </script>
    <?php
}




try{    
    // Je crée la connection avec la base de données
    $conn = new mysqli($serveur, $user, $pass, $dbname);
    // Je la verifie, si une erreur survient le procesus est arreté et la page renvoi le code erreur
    if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }

    //Je fais ma requete sql en inserant les données de mes variables

    $sql = "INSERT INTO form (nom, prenom, mail, msg)
    VALUES ('$nom', '$prenom', '$mail', '$msg')";

    if ($conn->query($sql) === TRUE) {
        //Si la requete s'effectue correctement on ouvre une popup pour remercier l'utilsateur et on le renvoi sur la page d'accueil
        ?>
        <script>
            alert("<?php echo htmlspecialchars('Merci pour votre message !', ENT_QUOTES); ?>")  
            window.location = '../site_accueil/accueil.php'; 
        </script>
        <?php
    } else {
        //Sinon on renvoi le message d'erreur
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    //Tout est fini on peux fermer la connection a la base de donnée
    $conn->close();

}
catch(PDOException $e){ //Si le script qui se trouve dans le try renvoi une erreur, on affiche l'erreur sans executer le try
    echo 'Impossible de traiter les données. Erreur : '.$e->getMessage();
}

?>
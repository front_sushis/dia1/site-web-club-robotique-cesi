<!DOCTYPE html>
<html>
    <head>
      <title>Contact</title>
      <meta charset="utf-8">
      <link rel="stylesheet" href="../site_contact/contact.css" />
      <video id="background-video" autoplay loop muted>
      <source src="../rsrc_videos/background.mp4" type="video/mp4"> <!--importe la video de fond-->
      </video>
      <link rel="preconnect" href="https://fonts.googleapis.com"/>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Source+Code+Pro:wght@300&family=Ubuntu:wght@300;400&display=swap"
      rel="stylesheet"
    />
    </head>
    
    <body>

    <header>
        <?php
            include '../site_structure/header.php'; //importe le fichier equipe.css
        ?>
    </header>

    <div class="screen_aspect"><!--Le screen aspect concerne la partie avec le titre(top_tabs) et le reste des contenus-->

        <div class="top_tabs">
    
          <div class="active_tab">Nous contacter
            <img src="../rsrc_icones/robot1.png" id="cross_close" />
          </div>
          <form class="form" action="action.php" method="POST">
            <input type="text" class="input" name="nom" id="nom" placeholder="Nom"> <!--emplacement pour entrer le nom-->
            <input type="text" class="input" name="prenom" id="prenom" placeholder="Prenom"> <!--emplacement pour entrer le prenom-->
            <input type="email" class="input" name="mail" id="mail" placeholder="Adresse mail"> <!--emplacement pour entrer le mail-->
            <textarea type="text" class="input" name="msg" id="msg" placeholder="Votre message" rows="5" cols="50"></textarea> <!--emplacement pour entrer le message-->
                <div class='bouton_ok'><!--bouton envoyer-->
                    <input type="submit" value="Envoyer">
                </div>
            <div class="inactive_tab"></div>
         </form>
        </div>
    </div>
    <?php include '../site_structure/bouton.html' ?> <!--Importe le bouton.html dans le dossier site_structure-->
    <footer>
        <?php
            include '../site_structure/footer.php'; 
        ?><!--Importe le footer.php dans le dossier site_structure-->
    </footer>
    </body>
    
</html>
